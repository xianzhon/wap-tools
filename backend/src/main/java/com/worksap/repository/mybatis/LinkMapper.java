package com.worksap.repository.mybatis;

import com.worksap.model.entity.Link;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface LinkMapper {

    public Integer selecCount();

    public List<Link> selectAll();

    public List<Link> selectByTagId(@Param("tagId") int tagId);

    public void addTags(@Param("tagIds") List<Integer> tagIds,@Param("linkId") int linkId);

    void deleteTagByLink(@Param("linkId")int linkId);

    void update(@Param("link") Link link);

    /**
     * insert a new link to DB
     * @param link it contains the auto-generated key which was fetched before
     */
    void saveLinkWithKey(@Param("link") Link link);

    /**
     * insert a new link to DB, in the same auto-generate the key
     * @param link whose key is null
     */
    void saveLink(@Param("link") Link link);

    Integer selectNextLinkKey();

}
