package com.worksap.repository.mybatis;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.worksap.model.entity.Tag;

@Mapper
public interface TagMapper {
    void deleteTagToLink(@Param("tagId") int tagId);
    void saveTag(@Param("tag")Tag tag);
}
