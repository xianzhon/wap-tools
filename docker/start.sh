#!/bin/bash 

container_name=wap-tools
host_ip=$(hostname -I | cut -f1 -d' ')
config_dir=$(pwd)/config

echo "re-deploy $container_name"
docker stop $container_name
docker rm $container_name

start_docker() {
		docker run -p 8088:8080 -d \
		--name=$container_name \
    --add-host=db:"${host_ip}" \
    -v "$config_dir:/config" \
		xianzhon/wap-tools

    docker exec -it $container_name /bin/bash /config/init-timezone.sh
}

start_docker
# http://localhost:8811/#/links
