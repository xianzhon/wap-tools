-- DB SQL for Sqlite

CREATE TABLE `link` (
  `id` integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  `name` text DEFAULT NULL,
  `href` text DEFAULT NULL,
  `status` tinyint DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL
);

CREATE TABLE `tag` (
  `id` integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  `name` text DEFAULT NULL,
  `color` text DEFAULT NULL,
  `sys` integer DEFAULT NULL
);


CREATE TABLE `tag_link` (
  `tag_id` integer NOT NULL,
  `link_id` integer NOT NULL,
  PRIMARY KEY (`tag_id`,`link_id`)
);


CREATE TABLE `text_resource` (
  `id` integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  `en` text DEFAULT NULL,
  `ja` text DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  `ip` text DEFAULT NULL
);

CREATE TABLE `user` (
  `id` integer NOT NULL PRIMARY KEY AUTOINCREMENT,
  `username` text DEFAULT NULL,
  `password` text DEFAULT NULL
);
